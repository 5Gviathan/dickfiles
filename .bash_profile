[[ -f ~/.bashrc ]] && . ~/.bashrc

export EDITOR="vim"
export DIFFPROG="vim -d"
export TERMINAL="termite"

[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg > /dev/null && exec startx -- vt1 &> /dev/null
