import XMonad
import System.IO                       (hPutStrLn)
import XMonad.Hooks.DynamicLog         (dynamicLogWithPP,ppSep,ppLayout,ppOutput)
import XMonad.Hooks.ManageDocks        (docks,avoidStruts)
import XMonad.Layout.Spacing           (spacing)
import XMonad.Layout.NoBorders         (noBorders)
import XMonad.Util.Run                 (spawnPipe)
import XMonad.Util.EZConfig            (additionalKeys)
import qualified XMonad.StackSet as W  (Stack,focusUp,swapMaster,swapDown,swapUp)

main = do
    xmproc <- spawnPipe "/usr/bin/xmobar ~/.xmonad/xmobarrc"
    xmonad $ docks defaultConfig
        { borderWidth        = 4
        , normalBorderColor  = "#7c6f64"
        , focusedBorderColor = "#a89984"
        , layoutHook         = (avoidStruts $ spacing 6 $ Tall 1 (2/100) (1/2)) ||| noBorders Full
        , modMask            = mod4Mask
        , logHook            = dynamicLogWithPP def        
                                 { ppSep    = "  "
                                 , ppLayout = const ""
                                 , ppOutput = hPutStrLn xmproc 
                                 }   
        }
        `additionalKeys`
        [ ((mod4Mask,               xK_Return), spawn "termite")
        , ((mod4Mask,               xK_Print ), spawn "teiler")
        , ((mod4Mask,               xK_Menu  ), spawn "rofi -show drun")
        , ((mod4Mask,               xK_l     ), spawn "slock")
        , ((mod4Mask .|. shiftMask, xK_Tab   ), windows W.focusUp   )
        , ((mod4Mask .|. shiftMask, xK_Return), windows W.swapMaster)
        , ((mod4Mask .|. shiftMask, xK_Up    ), windows W.swapDown  )
        , ((mod4Mask .|. shiftMask, xK_Down  ), windows W.swapUp    )
        , ((mod4Mask,               xK_Left  ), sendMessage Shrink  )
        , ((mod4Mask,               xK_Right ), sendMessage Expand  )
        ]
